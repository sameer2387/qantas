# Project Title
QANTAS API Project

## Versioning
qantas-0.0.1 (Beta Version)

## Authors
* **Sameer Gupta** - *Initial work*

## License
This project is licensed under no License

## Prerequisites
* Have Java Version 1.8.* in your system.
* Have Postman to test the APIs.
* Make sure port 8099 is not already used as this project runs on port 8099.

## Deployment
This project can be run using an executable war file present in the War Folder.
Copy the qantas-0.0.1.war file from WAR folder to anywhere in your system and execute below command:- 
* java -jar qantas-0.0.1.war

## Built With
* [Maven](https://maven.apache.org/) - Dependency Management

## Getting Started
Check below swagger urls to understand API definition (Note: Try below URLs after running the war file as mentioned above)
* URL for UI - http://localhost:8099/swagger-ui.html
* URL for JSON - http://localhost:8099/v2/api-docs

## About the Assignment 
1. API Contract Definition (SWAGGER is preferred but not mandatory)
* Please check URLs above in Getting Started section for checking Swagger.
2. Very high-level integration design document (in not more than 4 – 5 sentences or a
single diagram, no preference)
* Please check "QANTAS API INTEGRATION DESIGN" document in the project root folder.
3. Security mechanism (either implement or describe what would do if you had a
week’s time to deliver)
* Please check SecurityRestConfig class where as of now all APIs requests are authorized. Given more time we can implement 3 classes for security. 
4. APIs delivered through microservices pattern (either Java Spring Boot or NodeJS)
* APIs are delivered using JAVA Spring Boot. Check ProfileRestController for API code.
5. Checked in code in git
*  Code Checked in git. Clone using below details.
    * SSH - git@source.servian.com:sameergupta/qantas.git
    * HTTPS - https://source.servian.com/sameergupta/qantas.git
6. A readme file
* Current file.