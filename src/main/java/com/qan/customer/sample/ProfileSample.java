/***
Copyright  2019
*	 
*/

/***************
* CHANGE HISTORY
* ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * October 08, 2019     Sameer gupta  			Newly Added 
 */

package com.qan.customer.sample;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.qan.customer.model.Profile;

public class ProfileSample {

	public static Profile getSampleProfile() {
		Profile testProfile = new Profile(1);
		testProfile.setFirstName("sameer");
		testProfile.setLastName("gupta");
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = formatter.parse("23/05/1987");
		} catch (ParseException e) {
		}
		testProfile.setDateOfBirth(date);
		testProfile.setHomeAddress("My Home Address");
		testProfile.setOfficeAddress("My Office Address");
		testProfile.setEmailId("sameer.gupta@servian.com");
		return testProfile;
	}
}
