/***
Copyright  2019
*	 
*/

/***************
* CHANGE HISTORY
* ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * October 08, 2019     Sameer gupta  			Newly Added 
 */

package com.qan.customer.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.qan.customer.model.Profile;
import com.qan.customer.service.ProfileService;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
@RequestMapping("/rest/customer")
public class ProfileRestController {

	private ProfileService profileService;

	@Autowired(required = true)
	public void setProfileService(ProfileService profileService) {
		this.profileService = profileService;
	}

	@RequestMapping(value = "/profiles", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "listProfiles", response = Profile.class, responseContainer = "List")
	public ResponseEntity<Object> listProfiles() {
		List<Profile> listProfile;
		try {
			listProfile = this.profileService.getList();
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(listProfile, HttpStatus.OK);
	}

	@RequestMapping(value = "/profiles/{id}", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "getProfileById", response = Profile.class)
	public ResponseEntity<Object> getProfileById(@PathVariable(value = "id") int pProfileId) {
		Profile objProfile;
		try {
			objProfile = this.profileService.getById(pProfileId);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(objProfile, HttpStatus.OK);
	}

	@RequestMapping(value = "/profiles/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "deleteProfile", response = Profile.class)
	public ResponseEntity<Object> deleteProfile(@PathVariable("id") int pProfileId,
			RedirectAttributes redirectAttributes) {
		Profile objProfile;
		try {
			objProfile = this.profileService.delete(pProfileId);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(objProfile, HttpStatus.OK);
	}

	@RequestMapping(value = "/profiles", method = RequestMethod.POST, produces = "application/json")
	@ApiOperation(value = "createProfile", response = Profile.class)
	public ResponseEntity<Object> createProfile(@RequestBody Profile pProfile) {
		try {
			this.profileService.add(pProfile);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(pProfile, HttpStatus.OK);
	}

	@RequestMapping(value = "/profiles/{id}", method = RequestMethod.PUT, produces = "application/json")
	@ApiOperation(value = "updateProfile", response = Profile.class)
	public ResponseEntity<Object> updateProfile(@PathVariable("id") int pProfileId, @RequestBody Profile pProfile) {
		try {
			pProfile.setId(pProfileId);
			pProfile = this.profileService.update(pProfile);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(pProfile, HttpStatus.OK);
	}

}
