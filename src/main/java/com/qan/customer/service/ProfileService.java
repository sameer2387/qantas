/***
Copyright  2019
*	 
*/

/***************
* CHANGE HISTORY
* ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * October 08, 2019     Sameer gupta  			Newly Added 
 */

package com.qan.customer.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qan.customer.model.Profile;
import com.qan.customer.sample.ProfileSample;

@Service
public class ProfileService {

	public Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * To get all customer profiles
	 * 
	 * @return
	 */
	@Transactional
	public List<Profile> getList() {
		logger.info("Fetching all customer profile.");
		List<Profile> listProfile = new ArrayList<Profile>();
		listProfile.add(ProfileSample.getSampleProfile());
		return listProfile;
	}

	/**
	 * To get a customer profile
	 * 
	 * @param pProfileId
	 * @return
	 */
	@Transactional
	public Profile getById(int pProfileId) {
		logger.info("Fetching specific customer profile.");
		Profile pProfile = ProfileSample.getSampleProfile();
		return pProfile;
	}

	/**
	 * To delete a customer profile
	 * 
	 * @param pProfileId
	 * @return
	 */
	@Transactional
	public Profile delete(int pProfileId) {
		logger.info("Deleting customer profile.");
		Profile pProfile = ProfileSample.getSampleProfile();
		return pProfile;
	}

	/**
	 * To add new customer profile
	 * 
	 * @param pProfile
	 * @return
	 */
	@Transactional
	public Profile add(Profile pProfile) {
		logger.info("Adding new customer profile.");
		return pProfile;
	}

	/**
	 * To update existing customer profile
	 * 
	 * @param pProfile
	 * @return
	 */
	@Transactional
	public Profile update(Profile pProfile) {
		logger.info("Updating the customer profile.");
		return pProfile;
	}

}
