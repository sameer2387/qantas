/***
Copyright  2019
*	 
*/

/***************
* CHANGE HISTORY
* ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * October 08, 2019     Sameer gupta  			Newly Added 
 */

package com.qan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class QantasApplication {

	public static void main(String[] args) {
		SpringApplication.run(QantasApplication.class, args);
	}

	// URL for UI - http://localhost:8080/swagger-ui.html
	// URL for JSON - http://localhost:8080/v2/api-docs
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.qan"))
				.paths(PathSelectors.ant("/rest/**")).build();
		// .apiInfo(apiInfo())

	}
}
