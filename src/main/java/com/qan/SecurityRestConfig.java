/***
Copyright  2019
*	 
*/

/***************
* CHANGE HISTORY
* ==============
 *
 * DATE                 CHANGED BY              DESCRIPTION
 * October 08, 2019     Sameer gupta  			Newly Added 
 */

package com.qan;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class SecurityRestConfig extends WebSecurityConfigurerAdapter {

	// Given a time of 1 week we can implement all the below classes for security.
	// As of now everything is permitted to go through whereas in ideal scenario
	// only authenticated Users will be permitted.
	// @Autowired
	// CustomBasicAuthenticationEntryPoint customBasicAuthenticationEntryPoint;
	// @Autowired
	// private RESTAuthenticationFailureHandler authenticationFailureHandler;
	// @Autowired
	// private RESTAuthenticationSuccessHandler authenticationSuccessHandler;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable();

		http.antMatcher("/rest/**").authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
				// Later Only Authenticated Users will be allowed
				.antMatchers("/rest/customer/**").permitAll()
				// .antMatchers("/rest/customer/**").authenticated()
				.anyRequest().denyAll().and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				// .and().httpBasic().authenticationEntryPoint(customBasicAuthenticationEntryPoint)
				;

		// http.formLogin().successHandler(authenticationSuccessHandler);
		// http.formLogin().failureHandler(authenticationFailureHandler);
	}

}
